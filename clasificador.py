#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys

def es_correo_electronico(string):
    if "@" in string:
        list = string.split("@")
        if "." in list[1]:
            return True
        else:
            return False
    else:
        return False

def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def es_real(string):
    try:
        float(string)
        return True
    except ValueError:
        return False

def evaluar_entrada(string):
    print("El número pertenece a los reales. ")
    return " "

def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()